-- The pass rate is defined as the number of customers who pass both the KYC process divided
-- by the number of customers who attempt the process. Each customer has up to 2 attempts.

-- The pass rate has decreased substantially in the recent period. Please write a report that
-- outlines the root causes and solutions.

-- PASS RATE QUERIES
-- -----------------------------------------------------------

-- What is the pass rate per month, how much is it decreasing?
--
select DATE_FORMAT(created_at, '%Y-%m') AS `MONTH`, result, count(*) as `TOTAL` from doc_reports GROUP by `MONTH`, `result` order by `MONTH` DESC;


select DATE_FORMAT(created_at, '%Y-%m') AS `MONTH`, result, count(*) as `TOTAL` from facial_similarity_reports where result != '' GROUP by `MONTH`, `result` order by `MONTH` DESC;

-- One EMPTY result in facial ??? Why???
select * from facial_similarity_reports where (result is null or result = '');



-- view
show create view view_monthly_doc_results;
show create view view_monthly_facial_results;


-- COMBINED RESULTS
--
-- VIEW: report_combined_monthly_results;
select D.`MONTH`, D.`RESULT` as DOC_RESULT, F.`RESULT` as FACIAL_RESULT, D.TOTAL + F.TOTAL as COMBINED_TOTAL
	from (select `MONTH`, `RESULT`, `TOTAL` from view_monthly_doc_results) as D
	LEFT JOIN (
		select `MONTH`, `RESULT`, `TOTAL` from view_monthly_facial_results
	) AS F using(`MONTH`)
GROUP BY `MONTH`, DOC_RESULT, FACIAL_RESULT;


-- Both sets of data contain the same customers
select count(DISTINCT(user_id)) as `DOC_USERS` from doc_reports;
select count(DISTINCT(user_id)) as `FACIAL_USERS` from facial_similarity_reports;
select count(DISTINCT(D.USER_ID)) from doc_reports as D left join facial_similarity_reports as F using (user_id) where D.user_id = F.user_id;


-- Both sets of data contain the same attempts
select count(DISTINCT(attempt_id)) as `DOC_ATTEMPTS` from doc_reports;
select count(DISTINCT(attempt_id)) as `FACIAL_ATTEMPTS` from facial_similarity_reports;
select count(DISTINCT(D.attempt_id)) from doc_reports as D left join facial_similarity_reports as F using (attempt_id) where D.attempt_id = F.attempt_id;


-- cleared users
select distinct(user_id), DATE_FORMAT(created_at, '%Y-%m') as `MONTH` from doc_reports where `result` = 'clear';
select distinct(user_id), DATE_FORMAT(created_at, '%Y-%m') as `MONTH` from facial_similarity_reports where `result` = 'clear';
SELECT distinct(user_id) as USER_ID, `MONTH` from((
	select * from view_cleared_docs as D where D.user_id in (select user_id from view_cleared_facials)
)
union
(
	select * from view_cleared_facials as D where D.user_id in (select user_id from view_cleared_docs)
)) as UNITED;


-- uncleared users
select distinct(user_id), DATE_FORMAT(created_at, '%Y-%m') as `MONTH` from doc_reports where `result` != 'clear';
select distinct(user_id), DATE_FORMAT(created_at, '%Y-%m') as `MONTH` from facial_similarity_reports where `result` != 'clear';


-- Uncleared in both doc and facial search
SELECT distinct(user_id) as USER_ID, `MONTH` from((
	select * from view_uncleared_docs as D where D.user_id in (select user_id from view_uncleared_facials)
)
union
(
	select * from view_uncleared_facials as D where D.user_id in (select user_id from view_uncleared_docs)
)) as UNITED;

-- all uncleared
select distinct(user_id) as USER_ID, `MONTH` from ((
	select * from view_uncleared_docs
)
union
(
	select * from view_uncleared_facials
)) as UNITED;


-- All users
select distinct(user_id) as USER_ID, `FIRST_ATTEMPT` AS `MONTH`, `FIRST_ATTEMPT`, `LAST_ATTEMPT` from ((
	select distinct(user_id) as user_id, DATE_FORMAT(MIN(created_at), '%Y-%m') as `FIRST_ATTEMPT`, DATE_FORMAT(MAX(created_at), '%Y-%m') as `LAST_ATTEMPT` from doc_reports group by user_id
)
union
(
	select distinct(user_id) as user_id, DATE_FORMAT(MIN(created_at), '%Y-%m') as `FIRST_ATTEMPT`, DATE_FORMAT(MAX(created_at), '%Y-%m') as `LAST_ATTEMPT` from facial_similarity_reports group by user_id
)) as UNITED;



-- The pass rate is defined as the number of customers who pass both the KYC process divided
-- by the number of customers who attempt the process.
-- Attempted is all distinct user_id
-- pass = all users who have 'clear' result in both tables


select A.MONTH, A.NEW_USERS, P.TOTAL_PASSED, (A.NEW_USERS - P.TOTAL_PASSED) as TOTAL_FAILED, ((P.TOTAL_PASSED/A.NEW_USERS) * 100) as PASS_RATE
from (select `MONTH`, `NEW_USERS` from report_monthly_users) as A
left join (select `MONTH`, `TOTAL_PASSED` from report_monthly_cleared) AS P using (`MONTH`);



-- view users by clearance type
select distinct(user_id) as USER_ID, DATE_FORMAT(MIN(created_at), '%Y-%m') as `MONTH` from doc_reports group by USER_ID;
select distinct(user_id) as USER_ID, DATE_FORMAT(MIN(created_at), '%Y-%m') as `MONTH` from facial_similarity_reports group by user_id;





select A.MONTH, A.NEW_USERS, P.TOTAL_PASSED, (A.NEW_USERS - P.TOTAL_PASSED) as TOTAL_FAILED, ((P.TOTAL_PASSED/A.NEW_USERS) * 100) as PASS_RATE
from (select `MONTH`, `NEW_USERS` from report_monthly_users) as A
left join (select `MONTH`, `TOTAL_PASSED` from report_monthly_cleared) AS P using (`MONTH`);



-- Monthly passrate by docs

-- The pass rate is defined as the number of customers who pass both the KYC process divided
-- by the number of customers who attempt the process.
-- Attempted is all distinct user_id in docs
-- pass = all users who have 'clear' result in docs table
select A.MONTH, A.NEW_USERS, P.TOTAL_PASSED, (A.NEW_USERS - P.TOTAL_PASSED) as TOTAL_FAILED, ((P.TOTAL_PASSED/A.NEW_USERS) * 100) as PASS_RATE
from (select `MONTH`, count(*) as `NEW_USERS` from view_users_docs group by `MONTH`) as A
left join (select `MONTH`, count(*) as `TOTAL_PASSED` from view_cleared_docs group by `MONTH`) AS P using (`MONTH`);


-- Monthly passrate by facial

-- The pass rate is defined as the number of customers who pass both the KYC process divided
-- by the number of customers who attempt the process.
-- Attempted is all distinct user_id in docs
-- pass = all users who have 'clear' result in docs table
select A.MONTH, A.NEW_USERS, P.TOTAL_PASSED, (A.NEW_USERS - P.TOTAL_PASSED) as TOTAL_FAILED, ((P.TOTAL_PASSED/A.NEW_USERS) * 100) as PASS_RATE
from (select `MONTH`, count(*) as `NEW_USERS` from view_users_facial group by `MONTH`) as A
left join (select `MONTH`, count(*) as `TOTAL_PASSED` from view_cleared_facials group by `MONTH`) AS P using (`MONTH`);



-- What is the sub-reason of the docs having a lower passrate?

select DATE_FORMAT(created_at, '%Y-%m') as `MONTH`, sub_result, count(*) as NUM_REASON from doc_reports group by sub_result,`MONTH`;





-- clear
-- - If all underlying verifications pass, the overall sub result will be clear
-- rejected
-- - If the report has returned information where the check cannot be processed further (poor quality image or an unsupported document).
-- suspected
-- - If the document that is analysed is suspected to be fraudulent.
-- caution
-- - If any other underlying verifications fail but they don’t necessarily point to a fraudulent document (such as the name provided by the applicant doesn’t match the one on the document)

select
	A.`MONTH`,
	A.NEW_USERS,
	ifnull(R.TOTAL, 0) as REJECTED,
	ifnull(S.TOTAL, 0) AS SUSPECTED,
	ifnull(C.TOTAL, 0) AS CAUTION,


	round(ifnull((R.TOTAL/A.NEW_USERS) * 100, 0), 3) AS PERCENT_REJECTED,
	round(ifnull((S.TOTAL/A.NEW_USERS) * 100, 0), 3) AS PERCENT_SUSPECTED,
	round(ifnull((C.TOTAL/A.NEW_USERS) * 100, 0), 3) AS PERCENT_CAUTIONED

from (select `MONTH`, count(USER_ID) as `NEW_USERS` from view_users_docs group by `MONTH`) as A

left join (select * from view_failure_reason_docs where `sub_result` = 'rejected') as R using(`MONTH`)
left join (select * from view_failure_reason_docs where `sub_result` = 'suspected') as S using(`MONTH`)
left join (select * from view_failure_reason_docs where `sub_result` = 'caution') as C using(`MONTH`)

group by A.`MONTH`;


select `MONTH`, `TOTAL` from view_failure_reason_docs where `sub_result` = 'rejected';
select `MONTH`, `TOTAL` from view_failure_reason_docs where `sub_result` != 'clear' and sub_result = 'suspected';
select `MONTH`, `TOTAL` from view_failure_reason_docs where `sub_result` != 'clear' and sub_result = 'caution';




-- Why are the docs being cautioned, and rejected?

update doc_reports set visual_authenticity_result = null where visual_authenticity_result = '';
update doc_reports set image_integrity_result = null where image_integrity_result = '';
update doc_reports set face_detection_result = null where face_detection_result = '';
update doc_reports set image_quality_result = null where image_quality_result = '';
update doc_reports set supported_document_result = null where supported_document_result = '';
update doc_reports set conclusive_document_quality_result = null where conclusive_document_quality_result = '';
update doc_reports set colour_picture_result = null where colour_picture_result = '';
update doc_reports set data_validation_result = null where data_validation_result = '';
update doc_reports set data_consistency_result = null where data_consistency_result = '';
update doc_reports set data_comparison_result = null where data_comparison_result = '';
update doc_reports set police_record_result = null where police_record_result = '';
update doc_reports set compromised_document_result = null where compromised_document_result = '';



select
	DATE_FORMAT(created_at, '%Y-%m') as `MONTH`,
	sub_result,
	count(nullif(visual_authenticity_result, 'clear')) as visual_authenticity_result,
	count(nullif(image_integrity_result, 'clear')) as image_integrity_result,
	count(nullif(face_detection_result, 'clear')) as face_detection_result,
	count(nullif(image_quality_result, 'clear')) as image_quality_result,
	count(nullif(supported_document_result, 'clear')) as supported_document_result,
	count(nullif(conclusive_document_quality_result, 'clear')) as conclusive_document_quality_result,
	count(nullif(colour_picture_result, 'clear')) as colour_picture_result,
	count(nullif(data_validation_result, 'clear')) as data_validation_result,
	count(nullif(data_consistency_result, 'clear')) as data_consistency_result,
	count(nullif(data_comparison_result, 'clear')) data_comparison_result,
	count(nullif(police_record_result, 'clear')) as police_record_result,
	count(nullif(compromised_document_result, 'clear')) as compromised_document_result
from doc_reports
where
	result != 'clear'
group by `MONTH`, sub_result;




-- Make a nice report
select * from view_failure_counts_docs where `sub_result` = 'caution';
select * from view_failure_counts_docs where `sub_result` = 'suspected';
select * from view_failure_counts_docs where `sub_result` = 'rejected';




-- 4	"visual_authenticity_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 5	"image_integrity_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 6	"face_detection_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 7	"image_quality_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 8	"created_at"	"datetime"	"YES"	NULL	""	""	""
-- 9	"supported_document_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 10	"conclusive_document_quality_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 11	"colour_picture_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 12	"data_validation_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 13	"data_consistency_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 14	"data_comparison_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 15	"attempt_id"	"char(36)"	"YES"	NULL	""	""	""
-- 16	"police_record_result"	"varchar(100)"	"YES"	NULL	""	""	""
-- 17	"compromised_document_result"	"varchar(100)"	"YES"	NULL	""	""	""





select `MONTH`, count(*) as `NEW_USERS` from view_users_docs group by `MONTH`;

select `MONTH`, count(*) as `TOTAL_PASSED` from view_cleared_docs group by `MONTH`;



select `MONTH`, count(user_id) from view_users group by `MONTH`;

-- Each customer has up to 2 attempts.



select count(*) as TOTAL_PASSED, `MONTH` from view_cleared group by `MONTH`;
select count(*) as TOTAL_FAILED, `MONTH` from view_uncleared group by `MONTH`;


select count(user_id), `MONTH` from view_cleared group by `MONTH`; -- 3123



select USER_ID, COUNT(Attempt_id) AS ATTEMPTS, min(created_at) as FIRST_ATTEMPT, max(created_at) as LAST_ATTEMPT, result from doc_reports group by USER_ID order by created_at DESC;
select USER_ID, COUNT(Attempt_id) AS ATTEMPTS, min(created_at) as FIRST_ATTEMPT, max(created_at) as LAST_ATTEMPT, result from facial_similarity_reports group by USER_ID;











select user_id, result, ATTEMPTS from(
    select
      result, user_id, count(attempt_id) as ATTEMPTS,
      row_number() over(
        partition by user_id
        order by
          created_at desc
      ) as rn
    from
      doc_reports
      group by user_id, attempt_id
  ) x
 where x.rn = 2
 group by user_id;
