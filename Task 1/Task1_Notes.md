# Task 1: Problem Solving Notes

TASK: What is the problem you see when analyzing this data set?

## Conclusion

The most concerning problem from analysing the data is USER RETENTION.

----

## The Process

> NB: Please showcase all your work and the underlying assumptions.

> [Link to Transaction Dataset](https://docs.google.com/spreadsheets/d/1CWD_evKLO-Y907A9HiAMFSctFFJdGWGbeitcHYNc83I/edit#gid=131285632)
>
> DATA CONTAINS: The transaction volume per transaction type in your region.

Steps taken to reach conclusion:

- Visual examination of the provided spreadsheet
  - Make some initial assumptions about the data
- Import DATA to DATABASE for more in depth analysis (FILE Provided)
- Question the data, and make assumptions based on the results


### Assumtions about the data:

**- SAFE Assumption: `ID` represents a single transaction**
```gherkin
Given all ID's are unique
And each row is a transaction
Then ID represents a single transaction
```

**- SAFE Assumption: `USER_ID` represents an individual user**
```gherkin
Given USER_ID's are not UNIQUE
    And the column is named using the word "USER"
Then USER_ID represents a single USER
```

**- Assumption: Transactions are likely withdrawls**

> - Does it matter if the transactions are withdrawls or not?
>   - NO, after consideration it probably doesn't for this exercise

```gherkin
Given all transactions are positive
    And transaction types include ATM
    And ATM is assumed to be an in person cash withdrawl
Then transactions listed are likely withdrawls
```

**- Assumption: `AMOUNT` is bound to `CURRENCY`**

> - Does it matter if the AMOUNT is bound to CURRENCY?
>   - NO, after consideration it probably doesn't for this exercise

```gherkin
Given the AMOUNT and CURRENCY are provided in the dataset
    And the AMOUNT has no other indication of normalisation
Then AMOUNT should be bound to CURRENCY
```

**- Assumption: Different `CURRENCY` indicates different locations**

```gherkin
Given there are 2 different CURRENCIES (GBP and EUR)
    And presuming GBP is used primarily in Great Britain
    And presuming EUR is used primarily through the rest of EUROPE
    And ATM is presumed to be in person withdrawls
When the same USER_ID has ATM transactions GBP and EUR currencies
Then the USER should be in differnt locations for those transactions
```

**- Assumption: A user's first transaction implies a new user**

```gherkin
Given the dataset may or may not be complete
    And all transactions have a CREATED_DATE
    And there is no other indication of a users first transaction
Then presume the first recorded USER transaction is their first transaction
```

**- Assumption: A user becomes stale after 3 months of inactivity**

```gherkin
Given the provided data may or may not be complete
    And the calculated† average lifetime of an account is ~85 Days
    and 3 months is on average 90 days
When the USER is inactive for 3 or more months
Then Assume the USER has become stale
```

----

## Probing the DATA

### -- How many users?

```sql
select count(distinct(user_id)) from CSV_IMPORT;
```

| UNIQUE_USERS |
|--------------|
|         1134 |

----

### -- How many user accounts have only a single transaction?

```sql
select
  count(*) AS SINGLE_TRANSACTIONS
from
  (
    SELECT
      user_id
    from
      CSV_IMPORT
    group BY
      user_id
    having
      count(user_id) < 2
  ) as A;
```

| SINGLE_TRANSACTIONS |
|---------------------|
|                 230 |

----

### -- How many user accounts have multiple transactions?

```sql
select
  count(*) AS MULTIPLE_TRANSACTIONS
from
  (
    SELECT
      user_id
    from
      CSV_IMPORT
    group BY
      user_id
    having
      count(user_id) > 1
  ) as A;
```

| MULTIPLE_TRANSACTIONS |
|-----------------------|
|                   904 |

----

### -- What is the average transaction amount for multi-transaction users?

```sql
SELECT
  ROUND(MIN(AMOUNT), 2) as MIN_AMOUNT,
  ROUND(MAX(AMOUNT), 2) as MAX_AMOUNT,
  ROUND(AVG(AMOUNT), 2) as AVG_AMOUNT
from
  CSV_IMPORT
where
  USER_ID in (
    SELECT
      user_id
    from
      CSV_IMPORT
    group BY
      user_id
    having
      count(user_id) > 1
  );
```

| MIN_AMOUNT | MAX_AMOUNT | AVG_AMOUNT |
|------------|------------|------------|
|       0.02 |     349.98 |     175.80 |

----

### -- What is the average transaction amount for single-transaction users?

```sql
SELECT
  ROUND(MIN(AMOUNT), 2) as MIN_AMOUNT,
  ROUND(MAX(AMOUNT), 2) as MAX_AMOUNT,
  ROUND(AVG(AMOUNT), 2) as AVG_AMOUNT
from
  CSV_IMPORT
where
  USER_ID in (
    SELECT
      user_id
    from
      CSV_IMPORT
    group BY
      user_id
    having
      count(user_id) < 2
  );
```

| MIN_AMOUNT | MAX_AMOUNT | AVG_AMOUNT |
|------------|------------|------------|
|       1.41 |     345.69 |     174.28 |

----

### -- What is the average transaction amount for all users?

```sql
SELECT
  ROUND(MIN(AMOUNT), 2) as MIN_AMOUNT,
  ROUND(MAX(AMOUNT), 2) as MAX_AMOUNT,
  ROUND(AVG(AMOUNT), 2) as AVG_AMOUNT
from
  CSV_IMPORT;
```

| MIN_AMOUNT | MAX_AMOUNT | AVG_AMOUNT |
|------------|------------|------------|
|       0.02 |     349.98 |     175.77 |

----
### -- How many transactions have negative amounts?

```sql
select count(*) from CSV_IMPORT where AMOUNT < 0;
```

| count(*) |
|----------|
|        0 |

----

### -- What's the highest transaction amount, type, and user?

```sql
SELECT USER_ID, TYPE, AMOUNT from CSV_IMPORT order by amount desc limit 1;
```

| USER_ID                              | TYPE  | AMOUNT |
|--------------------------------------|-------|--------|
| 46172727-471c-4627-b706-1f9881a8e4d2 | TOPUP | 349.98 |

----

### -- How many transactions per type?

```sql
select
  TYPE,
  count(TYPE) as NUM_TRANSACTIONS,
  (
    (
      COUNT(TYPE) /(
        select
          count(*)
        from
          CSV_IMPORT
      )
    ) * 100
  ) as PERCENTAGE_OF_TRANSACTIONS
from
  CSV_IMPORT
group by
  TYPE;
```

| TYPE          | NUM_TRANSACTIONS | PERCENTAGE_OF_TRANSACTIONS |
|---------------|------------------|----------------------------|
| ATM           |             2357 |                    23.5700 |
| BANK_TRANSFER |             2371 |                    23.7100 |
| CARD_PAYMENT  |             2325 |                    23.2500 |
| P2P_TRANSFER  |              574 |                     5.7400 |
| TOPUP         |             2373 |                    23.7300 |

----

### -- Average value of transactions by type?

```sql
select
  TYPE,
  ROUND(MIN(AMOUNT), 2) as MIN_AMOUNT,
  ROUND(MAX(AMOUNT), 2) as MAX_AMOUNT,
  ROUND(AVG(AMOUNT), 2) as AVG_AMOUNT
from
  CSV_IMPORT
group by
  TYPE;
```

| TYPE          | MIN_AMOUNT | MAX_AMOUNT | AVG_AMOUNT |
|---------------|------------|------------|------------|
| ATM           |       0.03 |     349.33 |     174.67 |
| BANK_TRANSFER |       0.02 |     349.54 |     176.66 |
| CARD_PAYMENT  |       0.06 |     349.93 |     178.43 |
| P2P_TRANSFER  |       1.76 |     349.86 |     177.67 |
| TOPUP         |       0.12 |     349.98 |     172.90 |

----

### -- What's the average number of transactions by type per user?

```sql
select
  TYPE,
  COUNT(TYPE),
  COUNT(distinct(user_id)) as USERS_MADE_TRANSACTION_TYPE
from
  CSV_IMPORT
group by
  TYPE
order by
  type;
```

| TYPE          | COUNT(TYPE) | USERS_MADE_TRANSACTION_TYPE |
|---------------|-------------|-----------------------------|
| ATM           |        2357 |                         760 |
| BANK_TRANSFER |        2371 |                         756 |
| CARD_PAYMENT  |        2325 |                         764 |
| P2P_TRANSFER  |         574 |                         360 |
| TOPUP         |        2373 |                         743 |

----

### -- What time period do the transactions cover per user?

```sql
select
  USER_ID,
  MIN(created_date),
  MAX(created_date),
  datediff(MAX(created_date), MIN(created_date)) as PERIOD_DAYS
from
  CSV_IMPORT
group by
  USER_ID
order by
  PERIOD_DAYS DESC;
```

| USER_ID                              | MIN(created_date)   | MAX(created_date)   | PERIOD_DAYS |
|--------------------------------------|---------------------|---------------------|-------------|
| 20100a1d-12bc-41ed-a5e1-bc46216e9696 | 2016-01-09 12:16:52 | 2017-01-08 21:58:00 |         365 |
| 9e988c5b-71d0-4f3e-8f81-b3c30aa9c240 | 2016-01-10 16:38:54 | 2017-01-08 00:32:25 |         364 |
| 26e8432c-5fdb-4b67-82e6-18f341d511a1 | 2016-01-10 17:51:06 | 2017-01-06 10:09:24 |         362 |
| c483216d-b457-44cc-bdab-9a65f44c9620 | 2016-01-14 02:26:28 | 2017-01-08 08:08:03 |         360 |
| b1693eae-6a23-49af-be37-14ecdb77ab6f | 2016-01-13 10:34:24 | 2017-01-06 16:23:35 |         359 |
| 5fd70cca-1685-40ad-a312-7d387f6911a9 | 2016-01-15 01:16:09 | 2017-01-07 16:10:45 |         358 |
| 48eef0ad-9929-4944-9e51-35de36b67ffd | 2016-01-16 00:07:18 | 2017-01-07 10:26:09 |         357 |
| f54baeeb-7282-4d23-9bb7-e8396ce1b159 | 2016-01-09 11:37:59 | 2016-12-31 13:23:24 |         357 |
| 3f6bb28c-f945-4027-9178-747956c3ea58 | 2016-01-09 10:26:27 | 2016-12-29 08:41:15 |         355 |
| 1319cca9-02a7-4a15-8abb-48d4e08e5aa3 | 2016-01-09 04:00:34 | 2016-12-28 11:35:49 |         354 |

> RESULTS TRUNCATED

----

### -- What is the average number of days for user tranasctions?

```sql
select
  ROUND(AVG(diff.PERIOD_DAYS)) as AVG_TRANSACTION_DAYS,
  MIN(diff.PERIOD_DAYS),
  MAX(diff.PERIOD_DAYS)
from
  (
    select
      user_id,
      datediff(MAX(created_date), MIN(created_date)) as PERIOD_DAYS
    from
      CSV_IMPORT
    GROUP by
      USER_ID
  ) as diff;
```

| AVG_TRANSACTION_DAYS | MIN(diff.PERIOD_DAYS) | MAX(diff.PERIOD_DAYS) |
|----------------------|-----------------------|-----------------------|
|                   84 |                     0 |                   365 |

----

### -- What is the average number of user tranasctions per day?

```sql
select
  SUM(source.NUM_TRANSACTIONS) as TOTAL_TRANSACTIONS,
  SUM(source.PERIOD_DAYS) as TOTAL_DAYS,
  (
    SUM(source.NUM_TRANSACTIONS) / SUM(source.PERIOD_DAYS)
  ) as AVG_TRANSACTIONS_PER_DAY
from
  (
    select
      user_id,
      count(*) as NUM_TRANSACTIONS,
      datediff(MAX(created_date), MIN(created_date)) as PERIOD_DAYS
    from
      CSV_IMPORT
    group by
      USER_ID
    order by
      NUM_TRANSACTIONS DESC,
      PERIOD_DAYS DESC
  ) as source;
```

| TOTAL_TRANSACTIONS | TOTAL_DAYS | AVG_TRANSACTIONS_PER_DAY |
|--------------------|------------|--------------------------|
|              10000 |      95388 |                   0.1048 |

----

### -- What is the average transactions per day per user?

```sql
select
  source.user_id,
  source.NUM_TRANSACTIONS,
  source.PERIOD_DAYS,
  (source.NUM_TRANSACTIONS / source.PERIOD_DAYS) as AVG_TRANSACTIONS_PER_DAY
from
  (
    select
      user_id,
      count(USER_ID) as NUM_TRANSACTIONS,
      datediff(MAX(created_date), MIN(created_date)) as PERIOD_DAYS
    from
      CSV_IMPORT
    group by
      USER_ID
    order by
      NUM_TRANSACTIONS DESC,
      PERIOD_DAYS DESC
  ) as source;
```

| user_id                              | NUM_TRANSACTIONS | PERIOD_DAYS | AVG_TRANSACTIONS_PER_DAY |
|--------------------------------------|------------------|-------------|--------------------------|
| 06bb2d68-bf61-4030-8447-9de64d3ce490 |              132 |         287 |                   0.4599 |
| d35f19f3-d9ad-48bf-bd1e-90f3ba4f0b98 |              103 |         352 |                   0.2926 |
| d1bc3cd6-154e-479f-8957-a69cdf414462 |               95 |         338 |                   0.2811 |
| 0fe472c9-cf3e-4e43-90f3-a0cfb6a4f1f0 |               85 |         327 |                   0.2599 |
| 65ac0928-e17d-4636-96f4-ebe6bdb9c98d |               84 |         171 |                   0.4912 |
| 20100a1d-12bc-41ed-a5e1-bc46216e9696 |               78 |         365 |                   0.2137 |
| 59d3efe9-135b-4b44-ac59-80a13da8109a |               78 |         262 |                   0.2977 |
| 84e22e7f-f2ab-413c-969a-667c78d8885c |               77 |         211 |                   0.3649 |
| f54baeeb-7282-4d23-9bb7-e8396ce1b159 |               65 |         357 |                   0.1821 |
| 26e8432c-5fdb-4b67-82e6-18f341d511a1 |               61 |         362 |                   0.1685 |

> RESULTS TRUNCATED

----

### -- How many transactions per day by currency?

```sql
select
  CURRENCY,
  count(CURRENCY) as NUM_TRANSACTIONS,
  (
    (
      COUNT(CURRENCY) /(
        select
          count(*)
        from
          CSV_IMPORT
      )
    ) * 100
  ) as PERCENTAGE_OF_TRANSACTIONS
from
  CSV_IMPORT
group by
  CURRENCY;
```

| CURRENCY | NUM_TRANSACTIONS | PERCENTAGE_OF_TRANSACTIONS |
|----------|------------------|----------------------------|
| EUR      |             4851 |                    48.5100 |
| GBP      |             5149 |                    51.4900 |

----

### -- How many ATM transactions per day by GBP, user and where there were 10 or more transactions?

```sql
select
  USER_ID,
  CURRENCY,
  TYPE,
  TRANSACTIONS
from
  user_transactions
where
  CURRENCY = 'GBP'
  and TYPE = 'ATM'
  and transactions > 9
group by
  TYPE,
  CURRENCY,
  USER_ID;
```

| USER_ID                              | CURRENCY | TYPE | TRANSACTIONS |
|--------------------------------------|----------|------|--------------|
| 06bb2d68-bf61-4030-8447-9de64d3ce490 | GBP      | ATM  |           17 |
| 0fe472c9-cf3e-4e43-90f3-a0cfb6a4f1f0 | GBP      | ATM  |           11 |
| 59d3efe9-135b-4b44-ac59-80a13da8109a | GBP      | ATM  |           11 |
| 616a1ab0-31c5-4a8a-a0fe-0f7cb78c617f | GBP      | ATM  |           10 |
| 62aaa0fb-65ae-44b6-884d-56f38a302b3e | GBP      | ATM  |           10 |
| 65ac0928-e17d-4636-96f4-ebe6bdb9c98d | GBP      | ATM  |           15 |
| 74fdc60d-ee12-47c1-85f0-1c7dd1dbbf16 | GBP      | ATM  |           12 |
| d35f19f3-d9ad-48bf-bd1e-90f3ba4f0b98 | GBP      | ATM  |           10 |

----

### -- How many ATM transactions per day by EUR, user and where there were 10 or more transactions?

```sql
select
  USER_ID,
  CURRENCY,
  TYPE,
  TRANSACTIONS
from
  user_transactions
where
  CURRENCY = 'EUR'
  and TYPE = 'ATM'
  and transactions > 9
group by
  TYPE,
  CURRENCY,
  USER_ID;
```

| USER_ID                              | CURRENCY | TYPE | TRANSACTIONS |
|--------------------------------------|----------|------|--------------|
| 06bb2d68-bf61-4030-8447-9de64d3ce490 | EUR      | ATM  |           19 |
| 0fe472c9-cf3e-4e43-90f3-a0cfb6a4f1f0 | EUR      | ATM  |           10 |
| 59d3efe9-135b-4b44-ac59-80a13da8109a | EUR      | ATM  |           10 |
| 84e22e7f-f2ab-413c-969a-667c78d8885c | EUR      | ATM  |           10 |
| d1bc3cd6-154e-479f-8957-a69cdf414462 | EUR      | ATM  |           13 |
| d35f19f3-d9ad-48bf-bd1e-90f3ba4f0b98 | EUR      | ATM  |           14 |

----

### -- The top users with a cross section of different transaction types in both EUR and GBP

```sql
select
  users.user_id
from
  users
  left join view_atm_eur_user as EURATM on users.user_id = EURATM.USER_ID
  left join view_atm_gbp_user as GBPATM on users.user_id = GBPATM.USER_ID
  left join view_bank_transfer_eur_user as EURBT on users.user_id = EURBT.USER_ID
  left join view_bank_transfer_gbp_user as GBPBT on users.user_id = GBPBT.USER_ID
  left join view_card_eur_user as EURCARD on users.user_id = EURCARD.USER_ID
  left join view_card_gbp_user as GBPCARD on users.user_id = GBPCARD.USER_ID
where
  EURATM.TRANSACTIONS > 5
  and GBPATM.TRANSACTIONS > 5
  and EURBT.TRANSACTIONS > 5
  and GBPBT.TRANSACTIONS > 5
  and EURCARD.TRANSACTIONS > 5
  and GBPCARD.TRANSACTIONS > 5
  and EURCARD.TRANSACTIONS > 5
  and GBPCARD.TRANSACTIONS > 5
  and true;
```

| user_id                              |
|--------------------------------------|
| 06bb2d68-bf61-4030-8447-9de64d3ce490 |
| 20100a1d-12bc-41ed-a5e1-bc46216e9696 |
| 59d3efe9-135b-4b44-ac59-80a13da8109a |
| 65ac0928-e17d-4636-96f4-ebe6bdb9c98d |
| 84e22e7f-f2ab-413c-969a-667c78d8885c |
| d1bc3cd6-154e-479f-8957-a69cdf414462 |
| d35f19f3-d9ad-48bf-bd1e-90f3ba4f0b98 |

----

### -- Select the average user lifetime

```sql
select AVG(PERIOD_DAYS) as AVG_LIFETIME from z_calc_transaction_time_periods;
```

| AVG_LIFETIME |
|--------------|
|      84.1164 |

----

### -- Create a matrix of all user transactions

```sql
select
  users.user_id,
  (
    IFNULL(EURATM.TRANSACTIONS, 0) + IFNULL(GBPATM.TRANSACTIONS, 0) + IFNULL(EURBT.TRANSACTIONS, 0) + IFNULL(GBPBT.TRANSACTIONS, 0) + IFNULL(EURCARD.TRANSACTIONS, 0) + IFNULL(GBPCARD.TRANSACTIONS, 0) + IFNULL(EURP2P.TRANSACTIONS, 0) + IFNULL(GBPP2P.TRANSACTIONS, 0) + IFNULL(EURTOP.TRANSACTIONS, 0) + IFNULL(GBPTOP.TRANSACTIONS, 0)
  ) as TOTAL_TRANSACTIONS,
  ifnull(ATPD.AVG_TRANSACTIONS_PER_DAY, 0) as AVG_TRANSACTIONS_PER_DAY,
  ifnull(CTTP.PERIOD_DAYS, 0) as PERIOD_DAYS,
  ifnull(EURATM.TRANSACTIONS, 0) as EUR_ATM,
  ifnull(GBPATM.TRANSACTIONS, 0) as GDP_ATM,
  ifnull(GBPBT.TRANSACTIONS, 0) as GDP_BT,
  ifnull(EURBT.TRANSACTIONS, 0) as EUR_BT,
  ifnull(EURCARD.TRANSACTIONS, 0) as EUR_CARD,
  ifnull(GBPCARD.TRANSACTIONS, 0) as GBP_CARD,
  ifnull(EURP2P.TRANSACTIONS, 0) as EUR_P2P,
  ifnull(GBPP2P.TRANSACTIONS, 0) as GBP_P2P,
  ifnull(EURTOP.TRANSACTIONS, 0) as EUR_TOP,
  ifnull(GBPTOP.TRANSACTIONS, 0) as GBP_TOP
from
  users
  left join view_atm_eur_user as EURATM USING(USER_ID)
  left join view_atm_gbp_user as GBPATM USING(USER_ID)
  left join view_bank_transfer_eur_user as EURBT USING(USER_ID)
  left join view_bank_transfer_gbp_user as GBPBT USING(USER_ID)
  left join view_card_eur_user as EURCARD USING(USER_ID)
  left join view_card_gbp_user as GBPCARD USING(USER_ID)
  left join view_p2p_eur_user as EURP2P USING(USER_ID)
  left join view_p2p_gbp_user as GBPP2P USING(USER_ID)
  left join view_topup_eur_user as EURTOP USING(USER_ID)
  left join view_topup_gbp_user as GBPTOP USING(USER_ID)
  left join z_average_transactions_per_day AS ATPD USING(user_id)
  left join z_calc_transaction_time_periods AS CTTP USING(user_id);
```

| user_id                              | TOTAL_TRANSACTIONS | AVG_TRANSACTIONS_PER_DAY | PERIOD_DAYS | EUR_ATM | GDP_ATM | GDP_BT | EUR_BT | EUR_CARD | GBP_CARD | EUR_P2P | GBP_P2P | EUR_TOP | GBP_TOP |
|--------------------------------------|--------------------|--------------------------|-------------|---------|---------|--------|--------|----------|----------|---------|---------|---------|---------|
| 00a2e510-355c-4f46-898c-4155e327ccf3 |                  4 |                   0.0435 |          92 |       0 |       0 |      0 |      1 |        2 |        0 |       0 |       0 |       1 |       0 |
| 00a98617-0d8e-45d8-bca0-b67fe5d3471d |                 19 |                   0.0640 |         297 |       2 |       3 |      3 |      2 |        2 |        1 |       0 |       1 |       4 |       1 |
| 014b3a6c-5341-4620-ab9d-91ea69907c51 |                 14 |                   0.0633 |         221 |       2 |       1 |      0 |      2 |        0 |        2 |       0 |       4 |       3 |       0 |
| 0169a613-81c2-44d9-a90e-39e3cab34380 |                 11 |                   0.4400 |          25 |       2 |       2 |      1 |      3 |        1 |        0 |       0 |       0 |       2 |       0 |
| 017b6b45-9070-4d63-a191-70199c08ed37 |                  1 |                   0.0000 |           0 |       0 |       0 |      0 |      0 |        0 |        0 |       0 |       1 |       0 |       0 |

> RESULTS TRUNCATED

----

### -- User Risk Analysis

```sql
select
  `a`.`ALLTIME_USERS` - `z`.`STALE_USERS` AS `CURRENT_USERS`,
  `b`.`HIGH_RISK_USERS` AS `HIGH_RISK_USERS`,
  `c`.`MED_RISK_USERS` AS `MED_RISK_USERS`,
  `d`.`LOW_RISK_USERS` AS `LOW_RISK_USERS`,
  `z`.`STALE_USERS` AS `STALE_USERS`,
  if(
    `b`.`HIGH_RISK_USERS` + `c`.`MED_RISK_USERS` + `d`.`LOW_RISK_USERS` + `z`.`STALE_USERS` = `a`.`ALLTIME_USERS` = 1,
    'Yes',
    'No'
  ) AS `ALL_USERS_ACCOUNTED_FOR`
from
  (
    (
      (
        (
          (
            select
              1 AS `IDENT`,
              count(0) AS `ALLTIME_USERS`
            from
              `revolut`.`users`
          ) `A`
          join (
            select
              1 AS `IDENT`,
              count(0) AS `HIGH_RISK_USERS`
            from
              `revolut`.`view_risk_high`
          ) `B` on(`a`.`IDENT` = `b`.`IDENT`)
        )
        join (
          select
            1 AS `IDENT`,
            count(0) AS `MED_RISK_USERS`
          from
            `revolut`.`view_risk_med`
        ) `C` on(`a`.`IDENT` = `c`.`IDENT`)
      )
      join (
        select
          1 AS `IDENT`,
          count(0) AS `LOW_RISK_USERS`
        from
          `revolut`.`view_risk_low`
      ) `D` on(`a`.`IDENT` = `d`.`IDENT`)
    )
    join (
      select
        1 AS `IDENT`,
        count(0) AS `STALE_USERS`
      from
        `revolut`.`view_stale_users`
    ) `Z` on(`a`.`IDENT` = `z`.`IDENT`)
  )
```

| CURRENT_USERS | HIGH_RISK_USERS | MED_RISK_USERS | LOW_RISK_USERS | STALE_USERS | ALL_USERS_ACCOUNTED_FOR |
|---------------|-----------------|----------------|----------------|-------------|-------------------------|
|           616 |             154 |            302 |            160 |         518 | Yes                     |

----

### -- User Churn All Time

```sql
SELECT
  (
    SELECT
      count(user_id)
    FROM
      report_transactions
  ) AS TOTAL_USERS,
  (
    SELECT
      count(*)
    FROM
      report_transactions
    WHERE
      TOTAL_TRANSACTIONS < 5
      AND PERIOD_DAYS < 5
  ) AS CHURN,
  ROUND(
    (
      (
        SELECT
          count(*)
        FROM
          report_transactions
        WHERE
          TOTAL_TRANSACTIONS < 5
          AND PERIOD_DAYS < 5
      ) / (
        SELECT
          count(user_id)
        FROM
          report_transactions
      ) * 100
    ),
    2
  ) AS CHURN_PERCENTAGE;
```

| TOTAL_USERS | CHURN | CHURN_PERCENTAGE |
|-------------|-------|------------------|
|        1134 |   310 |            27.34 |

----

### -- New users per month and num transactions made per month

```sql
SELECT
  DATE_FORMAT(created_date, "%Y-%m") AS PERIOD,
  count(distinct(USER_ID)) as NUM_NEW_USERS,
  count(*) AS TOTAL_TRANSACTIONS
FROM
  CSV_IMPORT
GROUP BY
  PERIOD;
```

| PERIOD  | NUM_NEW_USERS | TOTAL_TRANSACTIONS |
|---------|---------------|--------------------|
| 2016-01 |            79 |                214 |
| 2016-02 |           105 |                284 |
| 2016-03 |           161 |                520 |
| 2016-04 |           159 |                497 |
| 2016-05 |           189 |                605 |
| 2016-06 |           230 |                628 |
| 2016-07 |           276 |                925 |
| 2016-08 |           338 |               1212 |
| 2016-09 |           349 |               1080 |
| 2016-10 |           379 |               1215 |
| 2016-11 |           388 |               1196 |
| 2016-12 |           421 |               1286 |
| 2017-01 |           160 |                338 |

----

### -- New Users per month

```sql
select
  MIN(DATE_FORMAT(created_date, "%Y-%m")) as FIRST_TRANSACTION,
  COUNT(DISTINCT(USER_ID)) as NEW_USERS
from
  CSV_IMPORT
group by
  DATE_FORMAT(created_date, "%Y-%m");
```

| MONTH   | NEW_USERS |
|---------|-----------|
| 2016-01 |        79 |
| 2016-02 |       105 |
| 2016-03 |       161 |
| 2016-04 |       159 |
| 2016-05 |       189 |
| 2016-06 |       230 |
| 2016-07 |       276 |
| 2016-08 |       338 |
| 2016-09 |       349 |
| 2016-10 |       379 |
| 2016-11 |       388 |
| 2016-12 |       421 |
| 2017-01 |       160 |

----

### -- User risk levels report

```sql
SELECT
  (A.ALLTIME_USERS - Z.STALE_USERS) as CURRENT_USERS,
  B.HIGH_RISK_USERS,
  C.MED_RISK_USERS,
  D.LOW_RISK_USERS,
  Z.STALE_USERS,
  IF(
    (
      (
        B.HIGH_RISK_USERS + C.MED_RISK_USERS + D.LOW_RISK_USERS + Z.STALE_USERS
      ) = A.ALLTIME_USERS
    ) = 1,
    'Yes',
    'No'
  ) as ALL_USERS_ACCOUNTED_FOR
FROM
  (
    select
      1 as IDENT,
      count(*) as ALLTIME_USERS
    from
      users
  ) AS A
  INNER JOIN (
    select
      1 as IDENT,
      LAST_USE AS HIGH_RISK_USERS
    from
      report_first_last_use
    where
      `MONTH` = (
        select
          date_format(max(CREATED_DATE) - interval 2 month, '%Y-%m')
        from
          csv_import
      )
  ) AS B using(IDENT)
  INNER JOIN (
    select
      1 as IDENT,
      LAST_USE AS MED_RISK_USERS
    from
      report_first_last_use
    where
      `MONTH` = (
        select
          date_format(max(CREATED_DATE) - interval 1 month, '%Y-%m')
        from
          csv_import
      )
  ) AS C using(IDENT)
  INNER JOIN (
    select
      1 as IDENT,
      LAST_USE AS LOW_RISK_USERS
    from
      report_first_last_use
    where
      `MONTH` = (
        select
          date_format(max(CREATED_DATE), '%Y-%m')
        from
          csv_import
      )
  ) AS D using(IDENT)
  INNER JOIN (
    select
      1 as IDENT,
      SUM(LAST_USE) AS STALE_USERS
    from
      report_first_last_use
    where
      `MONTH` <= (
        select
          date_format(max(CREATED_DATE) - interval 3 month, '%Y-%m') AS UPTO
        from
          csv_import
      )
  ) AS Z using(IDENT);
```

| CURRENT_USERS | HIGH_RISK_USERS | MED_RISK_USERS | LOW_RISK_USERS | STALE_USERS | ALL_USERS_ACCOUNTED_FOR |
|---------------|-----------------|----------------|----------------|-------------|-------------------------|
|           616 |             154 |            302 |            160 |         518 | Yes                     |

### -- More than one transaction

```sql
select user_id from report_transactions where TOTAL_TRANSACTIONS > 1;
```

### -- Second transaction date of users (offset?)

```sql
select
  user_id,
  created_date
from
  csv_import
where
  USER_ID in (
    select
      user_id
    from
      report_transactions
    where
      TOTAL_TRANSACTIONS > 1
  )
order by
  user_id;
```

```sql
select
  min(date_format(`CREATED_DATE`, '%Y-%m')) AS `MONTH`,
  count(distinct `USER_ID`) AS `ACTIVATED_USERS`
from
  `csv_import`
where
  USER_ID in (
    select
      distinct(user_id)
    from
      report_transactions
    where
      TOTAL_TRANSACTIONS > 1
  )
group by
  date_format(`CREATED_DATE`, '%Y-%m');
```

### -- Acquisitions

```sql
select
  `MONTH`,
  count(`USER_ID`) as USERS_ACQUIRED
from
  (
    select
      user_id,
      date_format(`CREATED_DATE`, '%Y-%m') as `MONTH`,
      row_number() over(
        partition by user_id
        order by
          created_date asc
      ) as rn
    from
      CSV_IMPORT
  ) x
where
  x.rn = 1
group by
  month;
```


### -- Activations

```sql
select
  `MONTH`,
  count(`USER_ID`) as USERS_ACTIVATED
from
  (
    select
      user_id,
      date_format(`CREATED_DATE`, '%Y-%m') as `MONTH`,
      row_number() over(
        partition by user_id
        order by
          created_date asc
      ) as rn
    from
      CSV_IMPORT
  ) x
where
  x.rn = 2
group by
  month;
```

```sql
select * from report_monthly_activations;
```

### -- Revenue turnover

#### -- by currency

```sql
select
  `MONTH`,
  CURRENCY,
  TURNOVER
from
  (
    select
      date_format(`CREATED_DATE`, '%Y-%m') as `MONTH`,
      CURRENCY,
      sum(AMOUNT) as TURNOVER,
      row_number() over(
        partition by CURRENCY
        order by
          created_date asc
      ) as rn
    from
      CSV_IMPORT
  ) x
where
  x.rn = 1
group by
  month,
  CURRENCY;
```

```sql
select
  date_format(`CREATED_DATE`, '%Y-%m') as `MONTH`,
  CURRENCY,
  ROUND(sum(AMOUNT), 2) as TURNOVER
from
  CSV_IMPORT
where
  currency = 'GBP'
group by
  currency,
  `MONTH`;
```

```sql
select
  date_format(`CREATED_DATE`, '%Y-%m') as `MONTH`,
  CURRENCY,
  ROUND(sum(AMOUNT), 2) as TURNOVER
from
  CSV_IMPORT
where
  currency = 'EUR'
group by
  currency,
  `MONTH`;
```

```sql
select
  GBP.MONTH,
  GBP.TURNOVER as GBP_TURNOVER,
  (0.01 * GBP.TURNOVER) ONE_PERCENT_GBP,
  (0.05 * GBP.TURNOVER) FIVE_PERCENT_GBP,
  (0.1 * GBP.TURNOVER) TEN_PERCENT_GBP,
  EUR.TURNOVER as EUR_TURNOVER,
  (0.01 * EUR.TURNOVER) ONE_PERCENT_EUR,
  (0.05 * EUR.TURNOVER) FIVE_PERCENT_EUR,
  (0.1 * EUR.TURNOVER) TEN_PERCENT_EUR
from
  view_monthly_transaction_value_eur as GBP
  left join view_monthly_transaction_value_gbp as EUR USING(MONTH);
```

```sql
select
  users.user_id
from
  users
  left join view_atm_eur_user as EURATM on users.user_id = EURATM.USER_ID
  left join view_atm_gbp_user as GBPATM on users.user_id = GBPATM.USER_ID
  left join view_bank_transfer_eur_user as EURBT on users.user_id = EURBT.USER_ID
  left join view_bank_transfer_gbp_user as GBPBT on users.user_id = GBPBT.USER_ID
  left join view_card_eur_user as EURCARD on users.user_id = EURCARD.USER_ID
  left join view_card_gbp_user as GBPCARD on users.user_id = GBPCARD.USER_ID
where
  EURATM.TRANSACTIONS > 5
  and GBPATM.TRANSACTIONS > 5
  and EURBT.TRANSACTIONS > 5
  and GBPBT.TRANSACTIONS > 5
  and EURCARD.TRANSACTIONS > 5
  and GBPCARD.TRANSACTIONS > 5
  and EURCARD.TRANSACTIONS > 5
  and GBPCARD.TRANSACTIONS > 5
  and true;
```

```sql
select
  USER_ID,
  SUM(`VALUE`) as VALUE_TURNOVER
from
  user_transactions
where
  user_id in (
    select
      user_id
    from
      users
  )
group by
  user_id
order by
  VALUE_TURNOVER DESC;
```

```sql
select
  users.user_id
from
  users
  left join view_atm_gbp_user as GBPATM using(USER_ID)
  left join view_bank_transfer_gbp_user as GBPBT using(USER_ID)
  left join view_card_gbp_user as GBPCARD using(USER_ID)
where
  GBPATM.TRANSACTIONS > 5
  and GBPBT.TRANSACTIONS > 5
  and GBPCARD.TRANSACTIONS > 5;
```

```sql
select
  `revolut`.`csv_import`.`CREATED_DATE` AS `CREATED`,
  `revolut`.`csv_import`.`USER_ID` AS `USER_ID`,
  `revolut`.`csv_import`.`CURRENCY` AS `CURRENCY`,
  `revolut`.`csv_import`.`TYPE` AS `TYPE`,
  count(`revolut`.`csv_import`.`TYPE`) AS `TRANSACTIONS`,
  ROUND(SUM(AMOUNT), 2) as `VALUE`
from
  `revolut`.`csv_import`
group by
  `revolut`.`csv_import`.`TYPE`,
  `revolut`.`csv_import`.`CURRENCY`,
  `revolut`.`csv_import`.`USER_ID`;
```

```sql
select
  `a`.`FIRST_TRANSACTION` AS `MONTH`,
  `a`.`FIRST_USERS` AS `FIRST_USE`,
  `b`.`LAST_USERS` AS `LAST_USE`
from
  (
    (
      (
        select
          `users`.`FIRST_TRANSACTION` AS `FIRST_TRANSACTION`,
          count(`users`.`USER_ID`) AS `FIRST_USERS`
        from
          `revolut`.`users`
        group by
          `users`.`FIRST_TRANSACTION`
      )
    ) `A`
    join (
      select
        `users`.`LAST_TRANSACTION` AS `LAST_TRANSACTION`,
        count(`users`.`USER_ID`) AS `LAST_USERS`
      from
        `revolut`.`users`
      group by
        `users`.`LAST_TRANSACTION`
    ) `B` on(`a`.`FIRST_TRANSACTION` = `b`.`LAST_TRANSACTION`)
  )
order by
  `a`.`FIRST_TRANSACTION` desc;
```

```sql
select
  *
from
  users
where
  user_id not in (
    select
      user_id
    from
      view_stale_users
  );
```

```sql
select
  *
from
  view_user_lifetime_turnover
where
  user_id in (
    select
      user_id
    from
      view_current_users
  );
```


### -- how many transactions are made by people who drop off vs people who continue? 484

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_stale_users
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) < 15
  ) as A;
```

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_stale_users
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) >= 15
  ) as A;
```

### -- how many transactions are made by people who drop off vs people who continue?

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_low
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) < 15
  ) as A;
```

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_low
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) >= 15
  ) as A;
```

### -- how many transactions are made by people who drop off vs people who continue? 95

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_med
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) < 15
  ) as A;
```

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_med
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) >= 15
  ) as A;
```

### -- how many transactions are made by people who drop off vs people who continue? 95

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_high
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) < 15
  ) as A;
```

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_risk_high
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) >= 15
  ) as A;
```


### -- how many transactions are made by people who drop off vs people who continue? 95

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_top_users
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) < 70
  ) as A;
```

```sql
select
  count(*)
from
  (
    select
      user_id
    from
      user_transactions
    where
      user_id in (
        select
          user_id
        from
          view_top_users
      )
    GROUP by
      USER_ID
    having
      SUM(TRANSACTIONS) >= 70
  ) as A;
```
